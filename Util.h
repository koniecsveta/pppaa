#ifndef RAYTRACER_UTIL_H
#define RAYTRACER_UTIL_H

inline float clamp(float x){ return x < 0.0f ? 0.0f : x > 1.0f ? 1.0f : x; }

// converts RGB float in [0,1] to RGB int in [0, 255] + performs gamma correction
inline int toInt(float x){ return int(pow(clamp(x), 1 / 2.2) * 255 + .5); }

#endif //RAYTRACER_UTIL_H
