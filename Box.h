#ifndef RAYTRACER_BOX_H
#define RAYTRACER_BOX_H


class Box {
public:
    float3 minimumBounds;
    float3 maximumBounds;
    float3 emission;
    float3 color;
    ReflectionType materialType;


    //http://www.gamedev.net/topic/495636-raybox-collision-intersection-point/
    __device__ float intersect(const Ray &r) const {
        float epsilon = 0.001f; // prevent self intersection

        float3 tmin = (minimumBounds - r.origin) / r.direction;
        float3 tmax = (maximumBounds - r.origin) / r.direction;

        float3 real_min = fminf(tmin, tmax);
        float3 real_max = fmaxf(tmin, tmax);

        float minmax = fminf(fminf(real_max.x, real_max.y), real_max.z);
        float maxmin = fmaxf(fmaxf(real_min.x, real_min.y), real_min.z);

        if (minmax >= maxmin) { return maxmin > epsilon ? maxmin : 0; }
        else return 0;
    }
    //// http://www.scratchapixel.com/lessons/3d-basic-rendering/minimal-ray-tracer-rendering-simple-shapes/ray-box-intersection
    __device__ float3 normalAtPoint(float3 &point){
        float3 normal = make_float3(0.0f,0.0f,0.0f);
        float epsilon = 0.001f;

        if (fabs(minimumBounds.x - point.x) < epsilon) normal = make_float3(-1, 0, 0);
        else if (fabs(maximumBounds.x - point.x) < epsilon) normal = make_float3(1, 0, 0);
        else if (fabs(minimumBounds.y - point.y) < epsilon) normal = make_float3(0, -1, 0);
        else if (fabs(maximumBounds.y - point.y) < epsilon) normal = make_float3(0, 1, 0);
        else if (fabs(minimumBounds.z - point.z) < epsilon) normal = make_float3(0, 0, -1);
        else normal = make_float3(0, 0, 1);

        return normal;
    }

};


#endif //RAYTRACER_BOX_H
