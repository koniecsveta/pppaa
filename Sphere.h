#ifndef RAYTRACER_SPHERE_H
#define RAYTRACER_SPHERE_H

#include <cuda_runtime.h>
#include "ReflectionType.h"
//#include "Constants.h"
#include "Ray.h"


class Sphere {
public:

    float radius;
    float3 position, emission, color;
    ReflectionType reflectionType;

    //ray/sphere intersection
    __device__ float intersect(const Ray &r) const {

        //returns distance t to intersection point, 0 if no hit
        //ray equation: p(x,y,z) = ray.origin + t*ray.direction
        //sphere equation: x^2 + y^2 + z^2 = radius^2
        //solve t^2*ray.direction*ray.direction + 2*t*(origin-p)*ray.direction + (origin-p)*(origin-p) - radius*radius = 0 (quadratic eq)
        //more details on Scratchapixel.com

        float3 op = position - r.origin;    // distance from ray.origin to center sphere
        float t, epsilon = 0.0001f;  //prevent floating point precision artefacts
        float b = dot(op, r.direction);
        float D = b*b - dot(op, op) + radius*radius;
        if (D < 0) return 0;
        else D = sqrtf(D);    // if D >= 0, pick closest point in front of ray origin
        return (t = b - D) > epsilon ? t : ((t = b + D) > epsilon ? t : 0); // pick closest point in front of ray origin
    }
};


#endif //RAYTRACER_SPHERE_H
