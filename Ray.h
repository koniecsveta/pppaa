#ifndef RAYTRACER_RAY_H
#define RAYTRACER_RAY_H

#include <cuda_runtime.h>

class Ray {
public:
    float3 origin;
    float3 direction;
    __device__ Ray(float3 o_, float3 d_) : origin(o_), direction(d_) {}
};


#endif //RAYTRACER_RAY_H
