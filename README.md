The code has 1 dependency: CUDA Toolkit. After installing it, make && ./raytracer should work.

- TODO: cleanup master ( now still 2 versions here //for CPU version uncomment commented lines in Makefile, make && ./raytracercpu should work.)

Different branches for different versions are used:

- rt_no_accel_serial : basic rt algorithm (primitives: spheres, boxes, triangles, triangle meshes;
materials: diffuse, reflective, "refractive", ray/triangle intersection done by Muller-Trumbore alg, 1 bounding box around the triangle mesh as primitive acceleration structure)

- rt_no_accel_gpu : same as rt_no_accel_serial but with some GPU optimisations (trinagle data in GPUs texture memory aligned in float4s for coalesced access, 1 thread/pixel rendering)

Scene with poorBunny.obj:

rt_no_accel_gpu branch version time: 30.00 seconds.

rt_no_accel_serial branch version time: 2601.00 seconds.

Scene with bunny.obj:

rt_no_accel_gpu branch version without BVH time: 191.00 seconds. (shadow ok)

rt_no_accel_serial branch version without BVH time: many seconds.