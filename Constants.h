#ifndef RAYTRACER_CONSTANTS_H
#define RAYTRACER_CONSTANTS_H

#include <cuda_runtime.h>
#include "Sphere.h"
#include "Box.h"

#define M_PI 3.14159265359f  // pi
#define screenwidth 512
#define screenheight 384
#define samples 1024

__constant__ float3 cameraInitOrigin = {50.0f, 50.0f, 295.0f};
__constant__ float3 cameraInitDirection =  {0.0, -0.05, -1};

// SCENE
__constant__ Sphere spheres[] = {
// { float radius, float3 position, float3 emission, float3 colour, ReflectionType material }
{ 10000, { 50.0f, 40.8f, -1060 }, { 0.0003, 0.01, 0.15 }, { 0.99f, 0.99f, 0.99f }, /*{ 0.175f, 0.175f, 0.25f }*/ DIFF },
{ 100000, { 50.0f, -100000, 0 }, { 0.0, 0.0, 0 }, /*{ 0.8f, 0.2f, 0.f }*/ {0.144f,0.238f,0.144f}, DIFF },
{ 110000, { 50.0f, -110048.5, 0 }, { 3.6, 2.0, 0.2 }, { 0.f, 0.f, 0.f }, DIFF },
{ 4e4, { 50.0f, -4e4 - 30, -3000 }, { 0, 0, 0 }, { 0.2f, 0.2f, 0.2f }, DIFF },
{ 82.5, { 30.0f, 180.5, 42 }, { 16, 12, 6 }, { .6f, .6f, 0.6f }, DIFF },
{ 12, { 115.0f, 10, 105 }, { 0.0, 0.0, 0.0 }, { 0.9f, 0.9f, 0.9f }, REFR },
{ 22, { 65.0f, 22, 24 }, { 0, 0, 0 }, { 0.9f, 0.9f, 0.9f }, SPEC },
};

__constant__ Box boxes[] = {
// { float3 minbounds, float3 maxbounds, float3 emission, float3 colour, ReflectionType material}
{ { 5.0f, 0.0f, 70.0f }, { 45.0f, 11.0f, 115.0f }, { .0f, .0f, 0.0f }, { 0.5f, 0.5f, 0.5f }, DIFF },
};

#endif //RAYTRACER_CONSTANTS_H
