// CPU VERSION

#include <iostream>
#include <cuda_runtime.h>
#include <vector_types.h>
#include <fstream>
#include "device_launch_parameters.h"
#include "cutil_math.h" //http://www.icmc.usp.br/~castelo/CUDA/common/inc/cutil_math.h
#include <curand.h>
#include <curand_kernel.h>
#include "Sphere.h"
#include <vector_functions.h>
#include "Constants.h"
#include "TriangleMesh.h"
#include "Util.h"
#include <time.h>
#include <stdio.h>

using namespace std;

int totalNumberOfTriangles = 0;

float3 sceneAABBoxMin;
float3 sceneAABBoxMax;

// instead of cuda texture:
vector<float4> triangles;

TriangleMesh mesh;

float *deviceTrianglePointer;


// http://www.cs.virginia.edu/~gfx/Courses/2003/ImageSynthesis/papers/Acceleration/Fast%20MinimumStorage%20RayTriangle%20Intersection.pdf
// http://www.scratchapixel.com/lessons/3d-basic-rendering/ray-tracing-rendering-a-triangle/moller-trumbore-ray-triangle-intersection
__host__ float RayTriangleIntersection(const Ray &r, const float3 &v0, const float3 &edge1, const float3 &edge2){
    float3 tvec = r.origin - v0;
    float3 pvec = cross(r.direction, edge2);
    float  det = dot(edge1, pvec);

    det = 1.0f/det; //__fdividef(1.0f, det);

    float u = dot(tvec, pvec) * det;

    if (u < 0.0f || u > 1.0f)
        return -1.0f;

    float3 qvec = cross(tvec, edge1);

    float v = dot(r.direction, qvec) * det;

    if (v < 0.0f || (u + v) > 1.0f)
        return -1.0f;

    return dot(edge2, qvec) * det;
}

// cross product of two triangle edges = vector orthogonal to triangle plane
__host__ float3 getTriangleNormal(const int triangleIndex){

    float4 edge1 = triangles[triangleIndex * 3 + 1];
    float4 edge2 = triangles[triangleIndex *3 +2];

    float3 triangleNormal = cross(make_float3(edge1.x, edge1.y, edge1.z), make_float3(edge2.x, edge2.y, edge2.z));
    triangleNormal = normalize(triangleNormal);

    return triangleNormal;
}




__host__ void intersectAllTriangles(const Ray& r, float& closestIntersectionFoundSoFar, int& triangleId, const int numberOfTriangles, int& geomtype){
    // i = triangle index
    for (int i = 0; i < numberOfTriangles; i++)
    {
        // triangles are stored in 1D texture using three consecutive float4s for each triangle,
        // first float4 contains the first vertex, second float4 contains the first precomputed edge, third float4 contains second precomputed edge like this:
        // (float4(vertex.x,vertex.y,vertex.z, 0), float4 (egde1.x,egde1.y,egde1.z,0),float4 (egde2.x,egde2.y,egde2.z,0))
        float4 v0 = triangles[i*3];
        float4 edge1 = triangles[i*3+1];
        float4 edge2 = triangles[i*3+2];

        float intersectionDistance = RayTriangleIntersection(r, make_float3(v0.x, v0.y, v0.z), make_float3(edge1.x, edge1.y, edge1.z), make_float3(edge2.x, edge2.y, edge2.z));

        if (intersectionDistance < closestIntersectionFoundSoFar && intersectionDistance > 0.001)
        {
            closestIntersectionFoundSoFar = intersectionDistance;
            triangleId = i;
            geomtype = 3;
        }
    }
}


__host__ inline bool intersectScene(const Ray &r, float &distanceToClosestIntersection, int &sphereId, int &boxId, int &triangleId, const int numberOfTriangles, int &geomtype, const float3 &boundingBoxMin, const float3 &boundingBoxMax){

    float inf = distanceToClosestIntersection=1e20;
    float distanceSpheres =1e21;
    float distanceBoxes = 1e21;


    //spheres
    float numberOfSpheres = sizeof(spheres) / sizeof(Sphere);
    for(int i = int(numberOfSpheres); i--; ) {
        if ((distanceSpheres = spheres[i].intersect(r)) && distanceSpheres < distanceToClosestIntersection) {
            distanceToClosestIntersection = distanceSpheres;
            sphereId = i;
            geomtype = 1;
        }
    }
    //boxes
    float numberOfBoxes = sizeof(boxes) / sizeof(Box);
    for (int i = int(numberOfBoxes); i--; ){
        if ((distanceBoxes = boxes[i].intersect(r)) && distanceBoxes < distanceToClosestIntersection){
            distanceToClosestIntersection = distanceBoxes;
            boxId = i;
            geomtype = 2;
        }
    }
    //triangles
    Box sceneBoundingBox;
    sceneBoundingBox.minimumBounds = boundingBoxMin;
    sceneBoundingBox.maximumBounds = boundingBoxMax;

    if(sceneBoundingBox.intersect(r)){

        intersectAllTriangles(r, distanceToClosestIntersection, triangleId, numberOfTriangles, geomtype);
    }

    //true if intersection occurred, false when no hit
    return distanceToClosestIntersection <inf;
}


// https://cgg.mff.cuni.cz/~jaroslav/teaching/2012-NPGR010/notes/
// solves the rendering equation:
// outgoing radiance = emitted radiance + reflected radiance
// reflected radiance = sum (integral) of incoming radiance from all directions in hemisphere above point,
// multiplied by reflectance function of material (BRDF) and cosine incident angle
__host__ float3 radiance(Ray r, curandState *curandState, const int totalNumberOfTriangles, const float3 &sceneAABBMin, const float3 &sceneAABBMax){ // returns ray color

    // accumulates ray color
    float3 accumulatedColor = make_float3(0.0f, 0.0f, 0.0f);
    float3 mask = make_float3(1.0f, 1.0f, 1.0f);

    // ray bounce loop
    for (int bounces = 0; bounces < 4; bounces++){

        float distanceToClosestIntersection;
        int sphereId = -1;
        int boxId = -1;
        int triangleId = -1;
        int geomtype = -1;
        ReflectionType reflectionType;
        float3 objectColour;
        float3 reflectedRayDirection;//ray direction of the next segment
        float3 intersectionPoint;
        float3 normal;
        float3 frontFacingNormal;
        float3 objectEmissionColor;

        // test ray for intersection with scene
        if (!intersectScene(r, distanceToClosestIntersection, sphereId, boxId, triangleId, totalNumberOfTriangles, geomtype, sceneAABBMin, sceneAABBMax)){
            return make_float3(0.0f, 0.0f, 0.0f); //black
           // printf("INFO[radiance]: no intersection with scene");
        }
        // else compute hitpoint and normal
        if(geomtype == 1){
            const Sphere &sphere = spheres[sphereId];
            intersectionPoint = r.origin + r.direction*distanceToClosestIntersection;
            normal = normalize(intersectionPoint - sphere.position);
            frontFacingNormal = dot(normal, r.direction) < 0 ? normal : normal * -1;
            reflectionType = sphere.reflectionType;
            objectColour = sphere.color;
            objectEmissionColor = sphere.emission;
            // add emission of current sphere to accumulated colour (first term in rendering equation sum)
            accumulatedColor += mask * objectEmissionColor;
        } else if (geomtype == 2) {
            Box &box = boxes[boxId];
            intersectionPoint = r.origin + r.direction*distanceToClosestIntersection;
            normal = normalize(box.normalAtPoint(intersectionPoint));
            frontFacingNormal = dot(normal, r.direction) < 0 ? normal : normal * -1;
            objectColour = box.color;
            reflectionType = box.materialType;
            objectEmissionColor = box.emission;
            accumulatedColor += mask * objectEmissionColor;
        } else if (geomtype == 3) {
            int triangleIndex = triangleId;
            intersectionPoint = r.origin + r.direction*distanceToClosestIntersection;
            normal = normalize(getTriangleNormal(triangleIndex));
            frontFacingNormal = dot(normal, r.direction) < 0 ? normal : normal * -1;
            objectColour = make_float3(0.9f,0.4f, 0.1f);
            reflectionType = DIFF;
            objectEmissionColor = make_float3(0.0f,0.0f,0.0f);
            accumulatedColor += (mask * objectEmissionColor);
        }

        // diffuse material reflects light uniformly in all directions
        // generate new diffuse ray:
        // origin = hitpoint of previous ray
        // random direction in hemisphere above hitpoint
        if(reflectionType == DIFF){
            // random number on unit circle (radius = 1, circumference = 2*Pi)
            float r1 = 2 * M_PI * curand_uniform(curandState);
            // pick random number for elevation
            float r2 = curand_uniform(curandState);
            float r2s = sqrtf(r2);

            // orthonormal basis u v w at hitpoint to use for calculation random ray direction
            // first vector = normal at hitpoint
            float3 w = frontFacingNormal;
            float3 u = normalize(cross((fabs(w.x) > .1 ? make_float3(0, 1, 0) : make_float3(1, 0, 0)), w));
            float3 v = cross(w,u);

            // compute random ray direction on hemisphere using polar coordinates
            // cosine weighted importance sampling (favours ray directions closer to normal direction)
            reflectedRayDirection = normalize(u*cos(r1)*r2s + v*sin(r1)*r2s + w*sqrtf(1 - r2));
            // prevent self intersection
            intersectionPoint+= frontFacingNormal*0.03;
            //multiply mask with colour of the object
            mask *=objectColour;
        }

        //mirror
        if(reflectionType == SPEC){
            //Snells law for reflectedRayDirection
            reflectedRayDirection = r.direction - 2.0f * normal * dot(normal, r.direction);
            intersectionPoint += frontFacingNormal * 0.01f;
            mask*=objectColour;
        }

        /********************** TODO *********************/
        //
        //https://graphics.stanford.edu/courses/cs148-10-summer/docs/2006--degreve--reflection_refraction.pdf
        //refraction (glass)
        if(reflectionType == REFR){
            bool into = dot(normal, frontFacingNormal)>0;//check whether ray is entering/leaving material
            float airIndexOfRefraction = 1.0f;
            float glassIndexOfRefraction = 1.5f;
            float refractiveMaterialsRatio = into ? airIndexOfRefraction / glassIndexOfRefraction : glassIndexOfRefraction / airIndexOfRefraction;
            float dotp = dot(r.direction, frontFacingNormal);//?????
            float cos2t = 1.0f - refractiveMaterialsRatio*refractiveMaterialsRatio * (1.f - dotp*dotp);

            if(cos2t < 0.0f) // total internal reflection = when the light ray attempts to leave material at too shallow angle and all the light is reflected
            {
                reflectedRayDirection = r.direction;//reflect(r.dir, n);
                reflectedRayDirection -= normal*2.0f*dot(normal, r.direction);
                intersectionPoint += frontFacingNormal * 0.01f;
            } else {
                float3 transmissionRayDirection = normalize(r.direction * refractiveMaterialsRatio - normal *((into ? 1 : -1 ) * (dotp*refractiveMaterialsRatio + sqrtf(cos2t))));
                float R0 = (glassIndexOfRefraction - airIndexOfRefraction)*(glassIndexOfRefraction - airIndexOfRefraction) / (glassIndexOfRefraction + airIndexOfRefraction) * (glassIndexOfRefraction + airIndexOfRefraction);
                //?????
                float c = 1.f - (into ? -dotp : dot(transmissionRayDirection, normal));
                float reflection = R0 + (1.f - R0) * c * c * c *c *c;
                float transmission = 1.f - reflection;
                float P = 0.25f + 0.5f + reflection;
                float RP = reflection / P;
                float TP = transmission / (1.f - P);


                //randomly choose reflection or transmission ray
                if(curand_uniform(curandState) < 0.25) // reflection
                {
                    mask *= RP;
                   reflectedRayDirection = reflect(r.direction, normal);
                    intersectionPoint += frontFacingNormal * 0.02f;
                } else {//transmission
                    mask *=transmission/P;//TP/(1-P)????;
                    reflectedRayDirection = transmissionRayDirection;
                    intersectionPoint += frontFacingNormal * 0.001f;
                }
            }

        }
        /*********************************************************/

    // new ray origin = intersection point of previous ray with scene
    r.origin = intersectionPoint;
    r.direction = reflectedRayDirection;
    }

    return accumulatedColor;
}


// executed on GPU / callable on CPU
// this runs in parallel on all threads
__host__ void render_kernel(float3 *output, const int totalNumberOfTriangles, float3 sceneAABBMin, float3 sceneAABBMax){
    int x, y;
    // assign a thread to every pixel (x,y)
    for(x=0;x<screenwidth;x++){
        for(y=0;y<screenheight;y++){

            // index of current pixel
            unsigned int i = (screenheight - y - 1) * screenwidth + x;

            curandState curandState;
            curand_init(/*threadId*/1, 0, 0, &curandState);

            // hardcoded camera ray(origin, direction)
            Ray camera(cameraInitOrigin,normalize(cameraInitDirection));
            //horizontal camera direction//
            float3 rayDirectionOffsetX = make_float3(screenwidth * 0.5 / screenheight, 0.0f, 0.0f);
            // 0.5 is FOV angle) //vertical vector of the camera
            float3 rayDirectionOffsetY = normalize(cross(rayDirectionOffsetX, camera.direction)) * 0.5; //cross gets vector perpendicular to rayDirectionOffsetX and cam.dir
            float3 finalPixelColor = make_float3(0.0f); // reset to zero for every pixel

            for (int s = 0; s < samples; s++){  // samples per pixel

                // direction from camera to pixel (x,y)
                // float3 primaryRayDirection = camera.direction + rayDirectionOffsetX * ((0.25 + x) / screenwidth - 0.5) + rayDirectionOffsetY * ((0.25 + y) / screenheight - 0.5);
                float3 primaryRayDirection = camera.direction + rayDirectionOffsetX * ((0.25*s/samples + x) / screenwidth - 0.5) + rayDirectionOffsetY * ((0.25*s/samples + y) / screenheight - 0.5);
                // float3 primaryRayDirection = camera.direction + rayDirectionOffsetX * ((0.25*curand_uniform(curandState) + x) / screenwidth - 0.5) + rayDirectionOffsetY * ((0.25*curand_uniform(curandState) + y) / screenheight - 0.5);

                // create primary ray, add incoming radiance to pixelcolor
                finalPixelColor = finalPixelColor + radiance(Ray(camera.origin, normalize(primaryRayDirection)), &curandState, totalNumberOfTriangles, sceneAABBMin, sceneAABBMax)*(1. / samples);
            }

            // write rgb value of pixel to image buffer on the GPU, clamp to [0, 1]
            output[i] = make_float3(clamp(finalPixelColor.x, 0.0f, 1.0f), clamp(finalPixelColor.y, 0.0f, 1.0f), clamp(finalPixelColor.z, 0.0f, 1.0f));
            // cout<<"INFO [render_kernel] pixel (" << x << ", " << y << ") = " << endl;
        }
    }
}

/*
void bindTriangles(float *deviceTrianglePointer, unsigned int numberOfTriangles)
{
    triangle_texture.normalized = false;                      // access with normalized texture coordinates
    triangle_texture.filterMode = cudaFilterModePoint;        // Point mode, so no
    triangle_texture.addressMode[0] = cudaAddressModeWrap;    // wrap texture coordinates

    size_t size = sizeof(float4)*numberOfTriangles * 3;
    cudaChannelFormatDesc channelDesc = cudaCreateChannelDesc<float4>();
    cudaBindTexture(0, triangle_texture, deviceTrianglePointer, channelDesc, size);
}
*/

//https://en.wikipedia.org/wiki/Wavefront_.obj_file
void loadMeshDataFromObjFile(const std::string filename, TriangleMesh &mesh){
    std::ifstream fileIn;
    fileIn.open(filename, std::ios_base::in);

    if(!fileIn.good()){
        cout << "ERROR [loadMeshDataFromObjFile]: File " << filename << " not good.\n";
        exit(0);
    }
    cout <<"INFO [loadMeshDataFromObjFile]: File " << filename << " is good.\n";
    char line[256];
    float f1, f2, f3;

    while (!fileIn.getline(line, 255).eof()){
        if (line[0] == 'v' && line[1] == ' '){
            if(sscanf(line, "v %f %f %f", &f1, &f2, &f3) == 3){
                mesh.vertices.push_back(make_float3(f1, f2, f3));
            } else {
                cout << "ERROR [loadMeshDataFromObjFile]: Reading optional coordinates is not supported.\n";
                exit(-1);
            }
        }
        if (line[0] == 'f' && line[1] == ' '){
            TriangleFace triangleFace;
            if (sscanf(line, "f %d %d %d", &triangleFace.vertexIndices[0], &triangleFace.vertexIndices[1], &triangleFace.vertexIndices[2]) != 3){
                cout << "ERROR [loadMeshDataFromObjFile]: Only triangle polygons are supported.\n";
                exit(-1);
            }
            mesh.faces.push_back(triangleFace);
        }

    }
    mesh.boundingBox[0] = make_float3(1000000,1000000,1000000);
    mesh.boundingBox[1] = make_float3(-1000000,-1000000,-1000000);
    for(int i = 0; i < mesh.vertices.size(); i++){
        mesh.boundingBox[0] = fminf(mesh.vertices[i], mesh.boundingBox[0]);
        mesh.boundingBox[1] = fmaxf(mesh.vertices[i], mesh.boundingBox[1]);
    }

    cout << "INFO [loadMeshDataFromObjFile]: " << filename << " file loaded.\n";
    cout << "INFO [loadMeshDataFromObjFile]: Number of faces:" << mesh.faces.size() << "\n";
    cout << "INFO [loadMeshDataFromObjFile]: Number of vertices:" << mesh.vertices.size() << "\n";
    cout << "INFO [loadMeshDataFromObjFile]: Mesh bounding box min:(" << mesh.boundingBox[0].x << "," << mesh.boundingBox[0].y << "," << mesh.boundingBox[0].z << ")\n";
    cout << "INFO [loadMeshDataFromObjFile]: Mesh bounding box max:(" << mesh.boundingBox[1].x << "," << mesh.boundingBox[1].y << "," << mesh.boundingBox[1].z << ")\n";
}


void loadTriangleMeshes(){
     loadMeshDataFromObjFile("bunny.obj",mesh);
   // loadMeshDataFromObjFile("poorBunny.obj",mesh); //poorBunny has less triangles

    float scaleFactor = 200;
    float3 offset = make_float3(90, 22, 100);

    for (unsigned int i = 0; i < mesh.faces.size(); i++)
    {
        // local copy of vertices
        float3 v0 = mesh.vertices[mesh.faces[i].vertexIndices[0] - 1];
        float3 v1 = mesh.vertices[mesh.faces[i].vertexIndices[1] - 1];
        float3 v2 = mesh.vertices[mesh.faces[i].vertexIndices[2] - 1];

        // scale
        v0 *= scaleFactor;
        v1 *= scaleFactor;
        v2 *= scaleFactor;

        // translate
        v0 += offset;
        v1 += offset;
        v2 += offset;

        // store triangle data as float4
        // store 1 vertex and 2 edges to save some calculations in ray-triangle intersection test
        triangles.push_back(make_float4(v0.x, v0.y, v0.z, 0));
        triangles.push_back(make_float4(v1.x - v0.x, v1.y - v0.y, v1.z - v0.z, 0));
        triangles.push_back(make_float4(v2.x - v0.x, v2.y - v0.y, v2.z - v0.z, 0));
    }

    // bounding box of mesh
    mesh.boundingBox[0] *= scaleFactor; mesh.boundingBox[0] += offset;
    mesh.boundingBox[1] *= scaleFactor; mesh.boundingBox[1] += offset;

    cout << "INFO [loadTriangleMeshesInCudaMemory]: Number of triangles check: " << mesh.faces.size() << " == " << triangles.size() / 3 << std::endl;

    // calculate total number of triangles
    size_t triangleSize = triangles.size() * sizeof(float4);
    totalNumberOfTriangles = triangles.size() / 3;

    sceneAABBoxMax = mesh.boundingBox[0];
    sceneAABBoxMin = mesh.boundingBox[1];
}


int main(){
    float3* outputMemoryHost = new float3[screenwidth*screenheight];
    time_t start,end;

    cout<<"INFO [main]: Gonna load triangles.\n";
    loadTriangleMeshes();
    cout<<"INFO [main]: Triangles loaded.\n";

    cout<<"INFO [main]: Rendering started.\n";
    time (&start);
    render_kernel(outputMemoryHost, totalNumberOfTriangles, sceneAABBoxMin,  sceneAABBoxMax);
    time (&end);
    double dif = difftime (end,start);

    printf ("INFO [main]: Elapsed time: %.2lf seconds.\n", dif );
    printf("INFO [main]: Image to be written to output.ppm.\n");
    //write output to ppm image, loop over pixels, write RGB values
    FILE *f = fopen("output.ppm", "w");
    fprintf(f, "P3\n%d %d\n%d\n", screenwidth, screenheight, 255);
    for (int i = 0; i< screenwidth*screenheight; i++)
        fprintf(f, "%d %d %d ", toInt(outputMemoryHost[i].x), toInt(outputMemoryHost[i].y), toInt(outputMemoryHost[i].z));

    cout<<"INFO [main]: Done.\n";

    delete[] outputMemoryHost;
}
