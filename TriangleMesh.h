#ifndef RAYTRACER_TRIANGLEMESH_H
#define RAYTRACER_TRIANGLEMESH_H

#include <vector>

class TriangleFace{
public:
    int vertexIndices[3];
};

class TriangleMesh{
public:
    std::vector<float3> vertices;
    std::vector<TriangleFace> faces;
    float3 boundingBox[2];
};

#endif //RAYTRACER_TRIANGLEMESH_H
